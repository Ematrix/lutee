package com.hl.mystudy.ui.studybinding;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.hl.mystudy.R;
import com.hl.mystudy.bean.User;
import com.hl.mystudy.databinding.FragmentStudyBinding2Binding;
import com.hl.mystudy.ui.studybinding.viewmodel.StudyBindingFragment2ViewModel;
import com.hl.mystudy.util.LogUtil;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/9 22:59
 * @since 1.0
 */
public class StudyBindingFragment2 extends Fragment {

    private static final String TAG = StudyBindingFragment2.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FragmentStudyBinding2Binding mBinding;
    private StudyBindingFragment2ViewModel mViewModel;
    private User mUser;
    private AppCompatEditText mEtView;


    private String mParam1;
    private String mParam2;

    public StudyBindingFragment2() {
        // Required empty public constructor
    }


    public static StudyBindingFragment2 newInstance(String param1, String param2) {
        StudyBindingFragment2 fragment = new StudyBindingFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.i("Enter MainFragment2::onCreate");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (inflater == null || getActivity() == null) {
            LogUtil.e("inflater == null || getActivity() == null");
            return null;
        }
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_study_binding2, container, false);
        mViewModel = new ViewModelProvider(this).get(StudyBindingFragment2ViewModel.class);
        mUser = new User("何林", 0, 43);
        mViewModel.setUser(mUser);
        mViewModel.getLiveDataUser().observe(getViewLifecycleOwner(), user -> {
            mBinding.setUser(mUser);
        });
        mEtView = mBinding.etFragment2;
        mEtView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (getActivity() != null) {
                    ((StudyBindingActivity) getActivity()).getmBinding().tvDataFragment2.setText(s.toString());
                }
            }
        });
        return mBinding.getRoot();
    }

    public void getResult(Callback callback) {
        if (callback != null) {
            callback.getResult(mEtView.getText().toString());
        }
    }


    /**
     * @author hyhdez
     * @des
     * @date 2023/9/9 23:24
     * @since 1.0
     */
    public interface Callback {
        /**
         * 返回结果字符串
         *
         * @param result result
         */
        void getResult(String result);
    }
}