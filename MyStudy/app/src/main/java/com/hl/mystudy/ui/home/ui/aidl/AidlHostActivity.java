package com.hl.mystudy.ui.home.ui.aidl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.hl.mystudy.R;

public class AidlHostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aidl_host);

        Intent intent = new Intent(this, MyAidlService.class);
        startService(intent);
    }
}