package com.hl.mystudy.ui.home.ui.viewpager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.hl.mystudy.R;

public class ViewPagerLazyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_lazy);
    }
}