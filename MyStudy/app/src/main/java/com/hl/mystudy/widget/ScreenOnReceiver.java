package com.hl.mystudy.widget;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

/**
 * @author hyhdez
 * @des
 * @date 2023/12/17 9:23
 */
public class ScreenOnReceiver extends BroadcastReceiver {
    private static final String TAG = ScreenOnReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
            Log.d(TAG, "屏幕亮了");
            Toast.makeText(context, "屏幕亮了", Toast.LENGTH_SHORT).show();
            startClockService(context);
        } else if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
            Log.d(TAG, "屏幕灭了");
        } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
            Log.d(TAG, "解锁屏幕");
            Toast.makeText(context, "解锁屏幕", Toast.LENGTH_SHORT).show();
            startClockService(context);
        } else if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.d(TAG, "启动完成");
            Toast.makeText(context, "启动完成", Toast.LENGTH_SHORT).show();
            startClockService(context);
        }
    }

    @Nullable
    private static ComponentName startClockService(Context context) {
        return context.startService(new Intent(context, ClockService.class));
    }
}
