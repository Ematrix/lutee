package com.hl.mystudy.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.hl.mystudy.R;

/**
 * @author hyhdez
 * @description : 音乐播放器的widget
 * @date 2024/1/14 15:07
 */
public class MusicWidgetProvider extends AppWidgetProvider {
    private static final String TAG = MusicWidgetProvider.class.getSimpleName();
    public static final String ACTION_PRE = "music.widget.action.PRE";
    public static final String ACTION_PLAY_PAUSE = "music.widget.action.PLAY_PAUSE";
    public static final String ACTION_NEXT = "music.widget.action.NEXT";
    public static final String ACTION_FAVORITE = "music.widget.action.FAVORITE";
    public static final String ACTION_LIST = "music.widget.action.LIST";

    private static final int ACTION_REQUEST_PRE = 1;
    private static final int ACTION_REQUEST_PLAY_PAUSE = 2;
    private static final int ACTION_REQUEST_NEXT = 3;
    private static final int ACTION_REQUEST_FAVORITE = 4;
    private static final int ACTION_REQUEST_LIST = 5;

    private Context mContext;


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "MusicWidgetProvider.onReceive Enter");
        mContext = context;
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (action == null) {
            return;
        }
        switch (action) {
            case ACTION_PRE:
                Log.e(TAG, "MusicWidgetProvider.onReceive ACTION_PRE");
                break;
            case ACTION_PLAY_PAUSE:
                Log.e(TAG, "MusicWidgetProvider.onReceive ACTION_PLAY_PAUSE");
                break;
            case ACTION_NEXT:
                Log.e(TAG, "MusicWidgetProvider.onReceive ACTION_NEXT");
                break;
            case ACTION_FAVORITE:
                Log.e(TAG, "MusicWidgetProvider.onReceive ACTION_FAVORITE");
                break;
            case ACTION_LIST:
                Log.e(TAG, "MusicWidgetProvider.onReceive ACTION_LIST");
                break;
            default:
                break;
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.e(TAG, "MusicWidgetProvider.onUpdate : Enter");
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Log.e(TAG, "MusicWidgetProvider.updateAppWidget Enter");
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.music_widget_provider);

//        byBroadCast(context, views);
        getService(views,ACTION_PRE,ACTION_REQUEST_PRE,R.id.mw_btn_pre);
        getService(views,ACTION_NEXT,ACTION_REQUEST_NEXT,R.id.mw_btn_next);
        getService(views,ACTION_PLAY_PAUSE,ACTION_REQUEST_PLAY_PAUSE,R.id.mw_btn_play_pause);
        getService(views,ACTION_FAVORITE,ACTION_REQUEST_FAVORITE,R.id.mw_btn_favorite);
        getService(views,ACTION_LIST,ACTION_REQUEST_LIST,R.id.mw_btn_list);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private void getService(RemoteViews views,String action,int requestCode,int id) {
        Intent intent = new Intent(mContext, MusicWidgetService.class);
        intent.setAction(action);
        PendingIntent pi = PendingIntent.getService(mContext,requestCode,intent,
                PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(id,pi);
    }

    private static void byBroadCast(Context context, RemoteViews views) {
        Intent intentPre = new Intent(context, MusicWidgetProvider.class);
        intentPre.setAction(ACTION_PRE);
        PendingIntent piPre = PendingIntent.getBroadcast(context, ACTION_REQUEST_PRE, intentPre,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.mw_btn_pre, piPre);

        Intent intentPause = new Intent(context, MusicWidgetProvider.class);
        intentPause.setAction(ACTION_PLAY_PAUSE);
        PendingIntent piPause = PendingIntent.getBroadcast(context, ACTION_REQUEST_PLAY_PAUSE, intentPause,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.mw_btn_play_pause, piPause);


        Intent intentNext = new Intent(context, MusicWidgetProvider.class);
        intentNext.setAction(ACTION_NEXT);
        PendingIntent piNext = PendingIntent.getBroadcast(context, ACTION_REQUEST_NEXT, intentNext,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.mw_btn_next, piNext);

        Intent intentFavorite = new Intent(context, MusicWidgetProvider.class);
        intentFavorite.setAction(ACTION_FAVORITE);
        PendingIntent piFavorite = PendingIntent.getBroadcast(context, ACTION_REQUEST_FAVORITE, intentFavorite,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.mw_btn_favorite, piFavorite);

        Intent intentList = new Intent(context, MusicWidgetProvider.class);
        intentList.setAction(ACTION_LIST);
        PendingIntent piList = PendingIntent.getBroadcast(context, ACTION_REQUEST_LIST, intentList,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.mw_btn_list,piList);
    }


    @Override
    public void onEnabled(Context context) {
        Log.e(TAG, "MusicWidgetProvider.onEnabled Enter");
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        Log.e(TAG, "MusicWidgetProvider.onDisabled Enter");
        // Enter relevant functionality for when the last widget is disabled
    }
}