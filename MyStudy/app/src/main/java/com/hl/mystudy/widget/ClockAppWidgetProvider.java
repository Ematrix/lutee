package com.hl.mystudy.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.hl.mystudy.R;
import com.hl.mystudy.ui.home.ui.okhttp.OkHttpStudyActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Implementation of App Widget functionality.
 */
public class ClockAppWidgetProvider extends AppWidgetProvider {
    private static final String TAG = ClockAppWidgetProvider.class.getSimpleName();
    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.CHINA);

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Log.e(TAG, "updateAppWidget updateView");
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        CharSequence widgetText = simpleDateFormat.format(date);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.clock_app_widget);
        views.setTextViewText(R.id.appwidget_text, widgetText);
        //点击跳转
        PendingIntent pendingIntent = getJumpIntentToCalender(context);
        views.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent);


        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    /**
     * 得到要跳转到日历的PendingIntent
     *
     * @param context 上下文
     * @return pendingIntent
     */
    private static PendingIntent getJumpIntentToCalender(Context context) {
        Intent clickIntent;
        clickIntent = new Intent();
        clickIntent.setComponent(new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity"));
        //跳到本应用没有问题,但是要重新添加小部件
//        clickIntent = new Intent(context, OkHttpStudyActivity.class);
        PendingIntent pendingIntent;
        int flag;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            flag = PendingIntent.FLAG_IMMUTABLE;
        } else {
            flag = PendingIntent.FLAG_UPDATE_CURRENT;
        }
        pendingIntent = PendingIntent.getActivity(context, 0, clickIntent, flag);
        return pendingIntent;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        //启动Service
        Log.e(TAG, "onEnabled");
        context.startService(new Intent(context, ClockService.class));
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        super.onDisabled(context);
        context.stopService(new Intent(context, ClockService.class));
    }
}