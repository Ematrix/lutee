package com.hl.mystudy.ui.home.ui.netrequest;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.hl.mystudy.R;
import com.hl.mystudy.bean.PicReturnData;
import com.hl.mystudy.databinding.ActivityStudyRetrofitBinding;
import com.hl.mystudy.net.INetApi;
import com.hl.mystudy.util.LogUtil;
import com.hl.mystudy.util.ToastUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudyRetrofitActivity extends AppCompatActivity {

    private StudyRetrofitViewModel mModel;
    private ActivityStudyRetrofitBinding mBinding;
    private Retrofit mRetrofit;
    private INetApi iNetApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_study_retrofit);
        mModel = new ViewModelProvider(this).get(StudyRetrofitViewModel.class);
        mModel.getReturnDataLive().observe(this, s -> {
            mBinding.tvResponse.setText(s);
        });

        mRetrofit = new Retrofit.Builder()
                .baseUrl("https://api.uomg.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        iNetApi = mRetrofit.create(INetApi.class);

        mBinding.btnGet.setOnClickListener(v -> {

            // TODO: 2023/9/13
            Call<PicReturnData> responseCall = iNetApi.get("男", "json");
            responseCall.enqueue(new Callback<PicReturnData>() {
                @Override
                public void onResponse(Call<PicReturnData> call, Response<PicReturnData> response) {
                    LogUtil.i("请求成功 response.body().toString() = : " + response.body().toString());
                    String s = response.body().toString();
                    ToastUtil.showToastShort(StudyRetrofitActivity.this, "请求成功!");
                    LogUtil.i("请求成功 : " + response.toString());
                    LogUtil.i("请求成功 message = : " + response.message());
                    LogUtil.i("请求成功 : s = " + s);
                    mModel.setReturnData(s);
                    showPic(response);

                }

                @Override
                public void onFailure(Call<PicReturnData> call, Throwable t) {
                    LogUtil.i("请求失败");
                    ToastUtil.showToastShort(StudyRetrofitActivity.this, "请求失败!");
                }
            });

        });

        mBinding.btnPost.setOnClickListener(v -> {
            // TODO: 2023/9/
            Call<PicReturnData> responseCall = iNetApi.get("男", "json");
            responseCall.enqueue(new Callback<PicReturnData>() {
                @Override
                public void onResponse(Call<PicReturnData> call, Response<PicReturnData> response) {
                    String s = response.body().toString();
                    mModel.setReturnData(s);
                    showPic(response);
                }

                @Override
                public void onFailure(Call<PicReturnData> call, Throwable t) {
                    ToastUtil.showToastShort(StudyRetrofitActivity.this, "请求失败!");
                }
            });
        });
    }

    private void showPic(Response<PicReturnData> response) {
        String url = response.body().getImgUrl();
        LogUtil.i("图片地址 : = " + url);
        Glide.with(mBinding.btnGet)
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.drawable.net_error_eror)
                .into(mBinding.ivPicture);
    }
}