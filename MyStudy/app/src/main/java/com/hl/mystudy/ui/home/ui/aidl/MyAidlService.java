package com.hl.mystudy.ui.home.ui.aidl;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.Nullable;

import com.hl.mystudy.IMyAidlInterface;
import com.hl.mystudy.util.LogUtil;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/18 11:04
 */
public class MyAidlService extends Service {

    IMyAidlInterface.Stub binder = new IMyAidlInterface.Stub() {
        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

        }

        @Override
        public void sayHello(String name) throws RemoteException {
            LogUtil.i(name);
        }

        @Override
        public int getName() throws RemoteException {
            return 1862021;
        }
    };

    // 不使用内部类的方式,外面的类里实现
    IMyAidlInterface.Stub binder2 = new MyAidlServiceImpl();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i("Host 启动服务成功");
        return super.onStartCommand(intent, flags, startId);
    }
}
