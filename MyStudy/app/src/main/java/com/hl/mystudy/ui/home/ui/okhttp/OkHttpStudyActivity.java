package com.hl.mystudy.ui.home.ui.okhttp;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.hl.mystudy.R;
import com.hl.mystudy.databinding.ActivityOkHttpStudyBinding;
import com.hl.mystudy.ui.home.ui.service.ServiceStudyActivity;
import com.hl.mystudy.util.LogUtil;

public class OkHttpStudyActivity extends AppCompatActivity {

    private ActivityResultLauncher<String> permissionLauncher;
    private ActivityResultLauncher<Intent> activityLauncher;
    private ActivityResultLauncher<String> mGetContent;
    private ActivityResultLauncher<String[]> openDocLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_http_study);
    }

}