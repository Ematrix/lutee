package com.hl.mystudy.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * @author Administrator
 */
public class PendingIntentAdapter {

    public static PendingIntent getActivityPi(Context context, int requestCode, Intent intent) {
        PendingIntent pi;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pi = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pi = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
        }
        return pi;
    }

    public static PendingIntent getBroadcastPi(Context context, int requestCode, Intent intent) {
        PendingIntent pi;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pi = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pi = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
        }
        return pi;
    }

}
