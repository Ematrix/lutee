package com.hl.mystudy.util;

import android.content.Context;
import android.widget.Toast;

/**
 * @author hyhdez
 * @des User类
 * showToast为单例，连续使用不会出现toast长时间呆在屏幕上的情况。<br/>
 * makeTextAndShow普通的Toast，将makeText和show连接起来。<br/>
 * @date 2023/9/9 10:16
 */
public class ToastUtil {

    public static Toast toast;

    public ToastUtil() {
        // 自动生成的构造函数存根
    }

    /**
     * 单例，连续使用不会出现toast长时间呆在屏幕上的情况，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param text    文本
     */
    public static void showToastShort(Context context, String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * 单例，连续使用不会出现toast长时间呆在屏幕上的情况，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param text    文本
     */
    public static void showToastLong(Context context, String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * 单例，连续使用不会出现toast长时间呆在屏幕上的情况，使用string资源，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param resId   文本id
     */
    public static void showToastShort(Context context, int resId) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, resId, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * 单例，连续使用不会出现toast长时间呆在屏幕上的情况，使用string资源，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param resId   文本id
     */
    public static void showToastLong(Context context, int resId) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, resId, Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * 普通的Toast，将makeText和show连接起来，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param text    文本
     */
    public static void makeTextAndShowShort(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 普通的Toast，将makeText和show连接起来，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param text    文本
     */
    public static void makeTextAndShowLong(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    /**
     * 普通的Toast，将makeText和show连接起来，使用string资源，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param resId   文本Id
     */
    public static void makeTextAndShowShort(Context context, int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
    }

    /**
     * 普通的Toast，将makeText和show连接起来，使用string资源，duration为Toast.LENGTH_SHORT
     *
     * @param context 上下文
     * @param resId   文本Id
     */
    public static void makeTextAndShowLong(Context context, int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_LONG).show();
    }

}
