package com.hl.mystudy.ui.home.ui.aidl;

import android.os.RemoteException;

import com.hl.mystudy.IMyAidlInterface;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/18 11:02
 */
public class MyAidlServiceImpl extends IMyAidlInterface.Stub {
    @Override
    public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

    }

    @Override
    public void sayHello(String name) throws RemoteException {

    }

    @Override
    public int getName() throws RemoteException {
        return 0;
    }
}
