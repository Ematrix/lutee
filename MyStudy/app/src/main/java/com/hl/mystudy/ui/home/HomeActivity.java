package com.hl.mystudy.ui.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.viewpager.widget.ViewPager;

import com.hl.mystudy.R;
import com.hl.mystudy.databinding.ActivityHomeBinding;
import com.hl.mystudy.ui.home.ui.dashboard.DashboardFragment;
import com.hl.mystudy.ui.home.ui.home.HomeFragment;
import com.hl.mystudy.ui.home.ui.notifications.NotificationsFragment;
import com.hl.mystudy.util.LogUtil;
import com.hl.mystudy.widget.ClockService;

import q.rorbin.badgeview.QBadgeView;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/10 10:57
 * @since 1.0
 */
public class HomeActivity extends AppCompatActivity {

    private DashboardFragment mDashboardFragment = DashboardFragment.newInstance(null);
    private HomeFragment mHomeFragment = HomeFragment.getInstance(null);
    private NotificationsFragment mNotificationsFragment = NotificationsFragment.getInstance(null);
    private ActivityHomeBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        initNavigation();
        initViewPager();
        initFragments();
        startService(new Intent(this, ClockService.class));
    }

    private void initFragments() {

    }

    private void initNavigation() {
        mBinding.navView.setOnItemSelectedListener(item -> {
            LogUtil.i(item.getItemId() + " selected");
            LogUtil.i(item.getOrder() + " selected");
            mBinding.viewPager.setCurrentItem(item.getOrder());

            BottomNavigationMenuView menuView = (BottomNavigationMenuView) mBinding.navView.getChildAt(0);
            View tab = menuView.getChildAt(item.getOrder());
            BottomNavigationItemView itemView = (BottomNavigationItemView) tab;

            new QBadgeView(this).bindTarget(itemView.getChildAt(0))
                    .setBadgeNumber(27)
                    .setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setBadgeTextColor(Color.YELLOW)
                    .setGravityOffset(0, 0, true);
            return true;
        });
    }

    private void initViewPager() {
        mBinding.viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return mHomeFragment;
                    case 1:
                        return mDashboardFragment;
                    case 2:
                        return mNotificationsFragment;
                    default:
                        return mHomeFragment;
                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        });
        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBinding.navView.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}