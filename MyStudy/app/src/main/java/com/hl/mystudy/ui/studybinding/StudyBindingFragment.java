package com.hl.mystudy.ui.studybinding;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hl.mystudy.R;
import com.hl.mystudy.util.LogUtil;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/9 22:59
 * @since 1.0
 */
public class StudyBindingFragment extends Fragment {

    private static final String TAG = StudyBindingFragment.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private TextView mTextView;


    private String mParam1;
    private String mParam2;

    public StudyBindingFragment() {
        // Required empty public constructor
    }


    public static StudyBindingFragment newInstance(String param1, String param2) {
        StudyBindingFragment fragment = new StudyBindingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.i("Enter MainFragment::onCreate");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_study_binding, container, false);
        mTextView = view.findViewById(R.id.tv_fragment);
        return view;
    }

    public void getResult(Callback callback) {
        if (callback != null) {
            callback.getResult(mTextView.getText().toString());
        }
    }


    /**
     * @author hyhdez
     * @des
     * @date 2023/9/9 23:24
     * @since 1.0
     */
    public interface Callback {
        /**
         * 返回结果字符串
         *
         * @param result result
         */
        void getResult(String result);
    }
}