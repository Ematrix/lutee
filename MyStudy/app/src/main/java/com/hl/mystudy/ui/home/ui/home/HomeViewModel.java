package com.hl.mystudy.ui.home.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/11 9:26
 * @since 1.0
 */
public class HomeViewModel extends ViewModel {

    private MutableLiveData<List<String>> mDataList = new MutableLiveData<>();

    public void setDataList(List<String> dataList) {
        mDataList.setValue(dataList);
    }

    public void postDataList(List<String> dataList) {
        mDataList.postValue(dataList);
    }

    public MutableLiveData<List<String>> getLiveDataList() {
        return mDataList;
    }

    public List<String> getDataList() {
        return mDataList.getValue();
    }

    public void setDataListTest() {
        List<String> dataList = new ArrayList<>(16);
        dataList.add("学习沉浸式状态栏和binding");
        dataList.add("ViewPager加载数据");
        dataList.add("ViewPager加载无限数据");
        dataList.add("ViewPager加载View");
        dataList.add("ViewPager无限循环");
        dataList.add("OKhttp");
        dataList.add("Retrofit");
        dataList.add("AIDL Host");
        dataList.add("AIDL Guest");
        dataList.add("服务学习");
        dataList.add("广播学习");
        dataList.add("OKHttp学习");
        dataList.add("IntentService 学习");
        dataList.add("测试自定义View");
        dataList.add("测试1");
        dataList.add("测试1");
        dataList.add("测试1");
        dataList.add("测试1");
        dataList.add("测试1");
        dataList.add("测试1");
        setDataList(dataList);
    }

}