package com.hl.mystudy.util;

import android.content.res.Resources;
import android.util.TypedValue;

/**
 * @author hyhdez
 * @des 屏幕密度相关
 * @date 2023/10/29 18:05
 */
@SuppressWarnings("unused")
public class DensityUtil {

    private static final float DENSITY = Resources.getSystem().getDisplayMetrics().density;

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     *
     * @param dp dp
     * @return px
     */
    public static int dp2px(float dp) {
        return (int) (0.5f + dp * DENSITY);
    }


    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     *
     * @param px px
     * @return dp
     */
    public float px2dp(float px) {
        return (px / DENSITY);
    }

    /**
     * 第二种方法
     *
     * @param dp dp
     * @return px
     */
    public static float dp2px2(float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
    }


    /**
     * sp转px
     *
     * @param sp sp
     * @return px
     */
    public static float sp2px(int sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().getDisplayMetrics());
    }

}
