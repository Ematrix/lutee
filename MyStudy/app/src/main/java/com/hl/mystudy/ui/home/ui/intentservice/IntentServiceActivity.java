package com.hl.mystudy.ui.home.ui.intentservice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;

import com.hl.mystudy.R;
import com.hl.mystudy.databinding.ActivityIntentServiceBinding;

public class IntentServiceActivity extends AppCompatActivity {
    private static final String TAG = IntentServiceActivity.class.getSimpleName();
    private ActivityIntentServiceBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_intent_service);

        mBinding.button.setOnClickListener(v -> {
            MyIntentService.startActionBaz(this, "BAZ","BAZ");
        });

        mBinding.button2.setOnClickListener(v -> {
            MyIntentService.startActionFoo(this, "FOO","FOO");
        });
    }
}