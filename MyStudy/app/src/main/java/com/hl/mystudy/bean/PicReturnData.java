package com.hl.mystudy.bean;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/13 12:14
 */
public class PicReturnData {
    //    {
//        "code": 1,
//            "imgurl": "https:\/\/ws2.sinaimg.cn\/large\/005zWjpngy1fuvgjtiihyj31400p0ajp.jpg"
//    }

    private String code;
    private String imgurl;

    public PicReturnData(String code, String imgUrl) {
        this.code = code;
        this.imgurl = imgUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImgUrl() {
        return imgurl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgurl = imgUrl;
    }

    @Override
    public String toString() {
        return "PicReturnData{" +
                "code=" + code +
                ", imgUrl='" + imgurl + '\'' +
                '}';
    }
}
