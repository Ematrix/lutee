package com.hl.mystudy.ui.studybinding.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hl.mystudy.bean.User;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/9 17:13
 */
public class StudyBindingFragment2ViewModel extends ViewModel {
    private MutableLiveData<User> mUser = new MutableLiveData<>();

    public MutableLiveData<User> getLiveDataUser(){
        return mUser;
    }

    public User getUser(MutableLiveData<User> mUser){
        return mUser.getValue();
    }

    public void setUser(User user) {
        mUser.setValue(user);
    }

    public void postUser(User user) {
        mUser.postValue(user);
    }

}
