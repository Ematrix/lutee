package com.hl.mystudy.ui.home.ui.viewpager;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.viewpager.widget.PagerAdapter;

import com.hl.mystudy.bean.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/13 21:02
 */
public class ViewPagerLoadDataViewModel extends ViewModel {
    private MutableLiveData<List<User>> userList = new MutableLiveData<>();

    public MutableLiveData<List<User>> getUserListLive() {
        return userList;
    }

    public void setData(List<User> dataList) {
        userList.setValue(dataList);
    }

    public void postData(List<User> dataList) {
        userList.setValue(dataList);
    }

    public List<User> getUserList() {
        return userList.getValue();
    }

    public List<User> getTestData() {
        List<User> dataList = new ArrayList<>(16);
        dataList.add(new User("姓名1", 0, 30));
        dataList.add(new User("姓名2", 0, 30));
        dataList.add(new User("姓名3", 0, 30));
        dataList.add(new User("姓名4", 0, 30));
        dataList.add(new User("姓名5", 0, 30));
        dataList.add(new User("姓名6", 0, 30));
        dataList.add(new User("姓名7", 0, 30));
        dataList.add(new User("姓名8", 0, 30));
        dataList.add(new User("姓名9", 0, 30));
        dataList.add(new User("姓名0", 0, 30));
        setData(dataList);

        return getUserList();
    }
}
