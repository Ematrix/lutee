package com.hl.mystudy.ui.home.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.hl.mystudy.R;
import com.hl.mystudy.databinding.ActivityTestFeatureViewBinding;

public class TestFeatureViewActivity extends AppCompatActivity {

    private ActivityTestFeatureViewBinding mBing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBing = DataBindingUtil.setContentView(this,R.layout.activity_test_feature_view);

//        mBing.mytv.setText("");
    }
}