package com.hl.mystudy.ui.home.ui.broadcast;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.hl.mystudy.R;

public class BroadcastStudyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_study);
    }
}