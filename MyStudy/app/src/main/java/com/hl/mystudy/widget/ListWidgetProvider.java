package com.hl.mystudy.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.hl.mystudy.R;
import com.hl.mystudy.ui.home.ui.okhttp.OkHttpStudyActivity;
import com.hl.mystudy.util.PendingIntentAdapter;
import com.hl.mystudy.util.ToastUtil;

/**
 * Implementation of App Widget functionality.
 */
public class ListWidgetProvider extends AppWidgetProvider {
    private static final String TAG = ListWidgetProvider.class.getSimpleName();
    public static final String CHANGE_IMAGE = "com.example.joy.action.CHANGE_IMAGE";
    public static final String ACTION_BTN1_CLICK = "com.example.joy.action.btn1.click";
    public static final String ACTION_BTN2_CLICK = "com.example.joy.action.btn2.click";
    private static RemoteViews mRemoteViews;
    private ComponentName mComponentName;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "ListWidgetProvider.onReceive Enter");
        super.onReceive(context, intent);

        // 接收按钮点击事件
        String action = intent.getAction();
        if (TextUtils.equals(CHANGE_IMAGE, action)) {
            Bundle extras = intent.getExtras();
            int position = extras.getInt(ListViewService.INITENT_DATA);
            // 点击事件的处理
            Log.e(TAG, "onReceive,点击了Item " + position);
        }else if (TextUtils.equals(ACTION_BTN2_CLICK, action)) {
            ToastUtil.showToastShort(context,"点击了Button2");
            Intent intentC = new Intent().setClassName("com.android.calendar", "com.android.calendar.homepage.AllInOneActivity");
            intentC.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentC);
        }
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.e(TAG, "ListWidgetProvider.onUpdate Enter");
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        Log.e(TAG, "ListWidgetProvider.updateAppWidget Enter");
        mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.list_widget_provider);
        mRemoteViews.setTextViewText(R.id.widget_btn1, context.getString(R.string.appwidget_btn_text));
        // 设置点击事件
        Intent skipIntent = new Intent(context, OkHttpStudyActivity.class);
        PendingIntent pi;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pi = PendingIntent.getActivity(context, 200, skipIntent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pi = PendingIntent.getActivity(context, 200, skipIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
        }
        mRemoteViews.setOnClickPendingIntent(R.id.widget_btn1, pi);

        Intent btn2Intent = new Intent(context, ListWidgetProvider.class);
        btn2Intent.setAction(ACTION_BTN2_CLICK);
        PendingIntent pi2 = PendingIntentAdapter.getBroadcastPi(context, 300, btn2Intent);
        mRemoteViews.setOnClickPendingIntent(R.id.widget_btn2, pi2);

        // 设置 ListView 的adapter。
        // (01) intent: 对应启动 ListViewService(RemoteViewsService) 的intent
        // (02) setRemoteAdapter: 设置 ListView 的适配器
        // 通过setRemoteAdapter将 ListView 和ListViewService关联起来，
        // 以达到通过 GridWidgetService 更新 gridview 的目的
        Intent lvIntent = new Intent(context, ListViewService.class);
        mRemoteViews.setRemoteAdapter(R.id.widget_lv, lvIntent);
        mRemoteViews.setEmptyView(R.id.widget_lv, android.R.id.empty);

        // 设置响应 ListView 的intent模板
        // 说明：“集合控件(如GridView、ListView、StackView等)”中包含很多子元素，如GridView包含很多格子。
        // 它们不能像普通的按钮一样通过 setOnClickPendingIntent 设置点击事件，必须先通过两步。
        // (01) 通过 setPendingIntentTemplate 设置 “intent模板”，这是必不可少的！
        // (02) 然后在处理该“集合控件”的RemoteViewsFactory类的getViewAt()接口中 通过 setOnClickFillInIntent 设置“集合控件的某一项的数据”

        /*
         * setPendingIntentTemplate 设置pendingIntent 模板
         * setOnClickFillInIntent   可以将fillInIntent 添加到pendingIntent中
         */
        Intent toIntent = new Intent(CHANGE_IMAGE);
        PendingIntent pendingIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(context, 200, toIntent, PendingIntent.FLAG_IMMUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity(context, 200, toIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
        }
        mRemoteViews.setPendingIntentTemplate(R.id.widget_lv, pendingIntent);

        appWidgetManager.updateAppWidget(appWidgetId, mRemoteViews);
    }

    @Override
    public void onEnabled(Context context) {
        Log.e(TAG, "ListWidgetProvider.onEnabled Enter");
    }

    @Override
    public void onDisabled(Context context) {
        Log.e(TAG, "ListWidgetProvider.onDisabled Enter");
    }
}