package com.hl.mystudy.util;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

import com.hl.mystudy.bean.SongInfo;

import java.util.ArrayList;
import java.util.List;

public class MusicManager {
    /**
     * 获取手机内的所有歌曲信息
     *
     * @param context
     * @return
     */
    @SuppressLint("Range")
    public static List<SongInfo> getSongList(Context context) {
        List<SongInfo> songList = new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        //从对应的数据库中获取对应列的歌曲信息
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.DATA
                }, null, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        if (cursor == null) {
            return songList;
        }
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            SongInfo song = new SongInfo();
            song.setTitle(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
            song.setArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
            song.setData(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
            songList.add(song);
            cursor.moveToNext();
        }
        cursor.close();
        return songList;
    }
}