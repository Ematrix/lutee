package com.hl.mystudy.ui.home.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hl.mystudy.BR;
import com.hl.mystudy.R;
import com.hl.mystudy.databinding.FragmentDashboardBinding;
import com.hl.mystudy.databinding.ItemFragmentDashboardRvItemBinding;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;

import java.util.List;

public class DashboardFragment extends Fragment {

    private FragmentDashboardBinding binding;
    private Bundle argBundle;
    private List<String> mDataList;

    public static DashboardFragment newInstance(Bundle bundle) {
        DashboardFragment dashboardFragment = new DashboardFragment();
        if (bundle != null) {
            dashboardFragment.setArguments(bundle);
        }
        return dashboardFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        argBundle = getArguments();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        dashboardViewModel.setDataListTest();
        mDataList = dashboardViewModel.getDataList();
        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        initRecyclerView();
        initSmartLayout();


        return root;
    }

    private void initSmartLayout() {
        SmartRefreshLayout refreshLayout = binding.smartRefreshLayoutDashboard;
        refreshLayout.setRefreshHeader(new ClassicsHeader(getActivity()));
        refreshLayout.setRefreshFooter(new ClassicsFooter(getActivity()));
        refreshLayout.setOnRefreshListener(refreshLayout1 -> {
            refreshLayout1.finishRefresh(2000/*,false*/);//传入false表示刷新失败
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout2 -> {
            refreshLayout2.finishLoadMore(2000/*,false*/);//传入false表示加载失败
        });
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = binding.rvDashboardFunc;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        DashBoardAdapter adapter = new DashBoardAdapter(mDataList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    private class DashBoardAdapter extends RecyclerView.Adapter<DashBoardViewHolder> {
        private List<String> mDataList;

        public DashBoardAdapter(List<String> dataList) {
            mDataList = dataList;
        }

        @NonNull
        @Override
        public DashBoardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.item_fragment_dashboard_rv_item, parent, false);
            DashBoardViewHolder viewHolder = new DashBoardViewHolder(binding);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull DashBoardViewHolder holder, int position) {
            // 两种方法设置数据,方法一,注意BR的作用
//            holder.getBinding().setVariable(BR.title, mDataList.get(position));
            // 方法二
            ((ItemFragmentDashboardRvItemBinding)holder.getBinding()).setTitle(mDataList.get(position));
            // 解决databinding闪烁问题
            holder.getBinding().executePendingBindings();
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }
    }


    private class DashBoardViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public DashBoardViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setBinding(ViewDataBinding binding) {
            this.binding = binding;
        }

        public ViewDataBinding getBinding() {
            return this.binding;
        }
    }
}