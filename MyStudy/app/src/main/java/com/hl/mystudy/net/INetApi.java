package com.hl.mystudy.net;


import com.hl.mystudy.bean.PicReturnData;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author hyhdez
 * @des retrofit 接口API
 * 域名是:<a href="https://api.uomg.com/">https://api.uomg.com/</a>
 * @date 2023/9/13 11:06
 */
public interface INetApi {

    /**
     * get请求随机竖版图片展示
     *
     * @param sort
     * @param format
     * @return
     */
    @GET("/api/rand.avatar")
    Call<PicReturnData> get(@Query("sort") String sort, @Query("format") String format);

    /**
     * Post请求随机竖版图片展示
     *
     * @param sort
     * @param format
     * @return
     */
    @FormUrlEncoded
    @POST("/api/rand.avatar")
    Call<PicReturnData> post(@Field("sort") String sort, @Field("format") String format);


}
