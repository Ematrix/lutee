package com.hl.mystudy.ui.home.ui.netrequest;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/13 8:27
 */
public class StudyRetrofitViewModel extends ViewModel {
    private MutableLiveData<String> returnData = new MutableLiveData<>();

    public void setReturnData(String data) {
        this.returnData.setValue(data);
    }

    public void postReturnData(String data) {
        this.returnData.postValue(data);
    }

    public MutableLiveData<String> getReturnDataLive() {
        return returnData;
    }

    public String getReturnData() {
        return returnData.getValue();
    }
}
