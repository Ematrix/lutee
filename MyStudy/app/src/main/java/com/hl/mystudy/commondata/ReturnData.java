package com.hl.mystudy.commondata;

/**
 * @author hyhdez
 * @des 接口返回的数据类
 * @date 2023/9/12 21:35
 */
public class ReturnData<T> {
    private int code;
    private String message;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}