package com.hl.mystudy.ui.home.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hl.mystudy.R;
import com.hl.mystudy.databinding.FragmentHomeBinding;
import com.hl.mystudy.ui.home.ui.TestFeatureViewActivity;
import com.hl.mystudy.ui.home.ui.aidl.AidlGuestActivity;
import com.hl.mystudy.ui.home.ui.aidl.AidlHostActivity;
import com.hl.mystudy.ui.home.ui.intentservice.IntentServiceActivity;
import com.hl.mystudy.ui.home.ui.netrequest.StudyRetrofitActivity;
import com.hl.mystudy.ui.home.ui.okhttp.OkHttpStudyActivity;
import com.hl.mystudy.ui.home.ui.viewpager.ViewPagerLoadDataActivity;
import com.hl.mystudy.ui.home.ui.viewpager.ViewPagerLoadViewActivity;
import com.hl.mystudy.ui.home.ui.viewpager.ViewPagerLazyActivity;
import com.hl.mystudy.ui.home.ui.viewpager.ViewPgerCycleActivity;
import com.hl.mystudy.ui.studybinding.StudyBindingActivity;
import com.hl.mystudy.util.ToastUtil;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;

import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private Bundle argBundle;
    private HomeViewModel mHomeViewModel;

    public static HomeFragment getInstance(Bundle bundle) {
        HomeFragment homeFragment = new HomeFragment();
        if (bundle != null) {
            homeFragment.setArguments(bundle);
        }
        return homeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        argBundle = getArguments();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mHomeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        initRecyclerView();
        initSmartRefreshLayout();
        return root;
    }

    private void initSmartRefreshLayout() {
        SmartRefreshLayout refreshLayout = binding.smartRefreshLayout;
        refreshLayout.setRefreshHeader(new ClassicsHeader(getActivity()));
        refreshLayout.setRefreshFooter(new ClassicsFooter(getActivity()));
        refreshLayout.setOnRefreshListener(refreshLayout1 -> {
            refreshLayout1.finishRefresh(2000/*,false*/);//传入false表示刷新失败
        });
        refreshLayout.setOnLoadMoreListener(refreshLayout2 -> {
            refreshLayout2.finishLoadMore(2000/*,false*/);//传入false表示加载失败
        });
    }


    private void initRecyclerView() {
        binding.rvList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        binding.rvList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        mHomeViewModel.setDataListTest();
        MyRecyclerAdapter myRecyclerAdapter = new MyRecyclerAdapter(mHomeViewModel.getDataList());
        myRecyclerAdapter.setOnItemClickListener(position -> {
            ToastUtil.showToastShort(getActivity(), "您点击了第" + position + "项");
            switch (position) {
                case 0:
                    startActivity(new Intent(getActivity(), StudyBindingActivity.class));
                    break;
                case 1://ViewPager加载数据
                    startActivity(new Intent(getActivity(), ViewPagerLoadDataActivity.class));
                    break;
                case 2://ViewPager加载无限数据
                    startActivity(new Intent(getActivity(), ViewPagerLazyActivity.class));
                    break;
                case 3://ViewPager加载View
                    startActivity(new Intent(getActivity(), ViewPagerLoadViewActivity.class));
                    break;
                case 4://ViewPager无限循环
                    startActivity(new Intent(getActivity(), ViewPgerCycleActivity.class));
                    break;
                case 5://OKhttp
                    startActivity(new Intent(getActivity(), OkHttpStudyActivity.class));
                    break;
                case 6://Retrofit
                    startActivity(new Intent(getActivity(), StudyRetrofitActivity.class));
                    break;
                case 7://AIDL Host
                    startActivity(new Intent(getActivity(), AidlHostActivity.class));
                    break;
                case 8://AIDL Guest
                    startActivity(new Intent(getActivity(), AidlGuestActivity.class));
                    break;
                case 9://服务学习
                    startActivity(new Intent(getActivity(), AidlGuestActivity.class));
                    break;
                case 10://广播学习
                    startActivity(new Intent(getActivity(), AidlGuestActivity.class));
                    break;
                case 11://OKHttp学习
                    startActivity(new Intent(getActivity(), AidlGuestActivity.class));
                    break;
                case 12://IntentService 学习
                    startActivity(new Intent(getActivity(), IntentServiceActivity.class));
                    break;
                case 13://测试自定义View
                    startActivity(new Intent(getActivity(), TestFeatureViewActivity.class));
                    break;
                default:
                    ToastUtil.showToastShort(getActivity(), "开发中....");
                    break;
            }
        });
        binding.rvList.setAdapter(myRecyclerAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /**
     * @author hyhdez
     * @des
     * @date 2023/9/11 10:09
     * @since 1.0
     */
    private class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

        private OnItemClickListener itemClickListener;
        private List<String> mDataList;

        public MyRecyclerAdapter(List<String> mDataList) {
            this.mDataList = mDataList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // 实例化展示的view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fragment_home_rv_item, parent, false);
            // 实例化viewholder
            MyViewHolder viewHolder = new MyViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.tv_item.setText(mDataList.get(position));
            holder.tv_item.setOnClickListener(v -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }


        private class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView tv_item;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_item = itemView.findViewById(R.id.tv_item);

            }
        }

        public void setOnItemClickListener(OnItemClickListener listener) {
            this.itemClickListener = listener;
        }


        public interface OnItemClickListener {
            void onItemClick(int position);
        }
    }


}