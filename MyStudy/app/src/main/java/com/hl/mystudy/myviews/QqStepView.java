package com.hl.mystudy.myviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.hl.mystudy.R;

/**
 * @author hyhdez
 * @des
 * @date 2023/10/29 19:51
 */
public class QqStepView extends View {

    private Paint mPaint;
    private int mOutColor = Color.RED;
    private int mInColor = Color.GREEN;
    private int mBorderWidth = 3;
    private int mTextSize = 15;
    private int mTextColor = Color.YELLOW;

    public QqStepView(Context context) {
        this(context,null);
    }

    public QqStepView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public QqStepView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.QqStepView);
        mOutColor = typedArray.getColor(R.styleable.QqStepView_qqStepOutColor, mOutColor);
        mInColor = typedArray.getColor(R.styleable.QqStepView_qqStepInColor, mInColor);
        mBorderWidth = typedArray.getDimensionPixelSize(R.styleable.QqStepView_qqStepBorderWidth, mBorderWidth);
        mTextSize = typedArray.getDimensionPixelSize(R.styleable.QqStepView_qqStepTextSized,mTextSize);
        mTextColor = typedArray.getColor(R.styleable.QqStepView_qqStepTextColor, mTextColor);
        typedArray.recycle();
        mPaint = new Paint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
