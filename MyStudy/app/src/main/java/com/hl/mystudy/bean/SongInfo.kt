package com.hl.mystudy.bean

class SongInfo {
    var title: String? = null //歌曲名
    var artist: String? = null //歌手
    var data: String? = null //歌曲所在路径
}