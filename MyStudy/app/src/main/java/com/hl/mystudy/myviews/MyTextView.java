package com.hl.mystudy.myviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.hl.mystudy.R;
import com.hl.mystudy.util.DensityUtil;
import com.hl.mystudy.util.LogUtil;

/**
 * @author hyhdez
 * @des 自定义的TextView
 * @date 2023/10/26 15:42
 */
public class MyTextView extends View {

    private CharSequence mText;
    private int mTextColor = Color.BLACK;
    private int mTextSize = 15;

    private Paint mPaint;
    private Rect mBounds = new Rect();

    public MyTextView(Context context) {
        this(context, null);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyTv);

        // 获取文本
        mText = typedArray.getText(R.styleable.MyTv_myTvText);
        // 获取文字颜色
        mTextColor = typedArray.getColor(R.styleable.MyTv_myTvTextColor, mTextColor);
        // 获取文字大小
        mTextSize = typedArray.getDimensionPixelSize(R.styleable.MyTv_myTvTextSize, mTextSize);
        mTextSize = DensityUtil.dp2px(mTextSize);
        // 回收
        typedArray.recycle();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(mTextSize);
        mPaint.setColor(mTextColor);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int modeWidth = MeasureSpec.getMode(widthMeasureSpec);
        int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);


        if (modeWidth == MeasureSpec.AT_MOST) {
            mPaint.getTextBounds(mText.toString(), 0, mText.length(), mBounds);
            width = mBounds.width() + getPaddingStart() + getPaddingEnd();
        }

        if (modeHeight == MeasureSpec.AT_MOST) {
            mPaint.getTextBounds(mText.toString(), 0, mText.length(), mBounds);
            height = mBounds.height() + getPaddingTop() + getPaddingBottom();
        }
        LogUtil.e("getPaddingStart() = " + getPaddingStart());
        LogUtil.e("getPaddingEnd() = " + getPaddingEnd());
        LogUtil.e("getPaddingLeft() = " + getPaddingLeft());
        LogUtil.e("getPaddingRight() = " + getPaddingRight());
        LogUtil.e("getPaddingTop() = " + getPaddingTop());
        LogUtil.e("getPaddingBottom() = " + getPaddingBottom());
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getPaddingStart();
        int y = getBaseline(mPaint);
        canvas.drawText(mText.toString(), x, y, mPaint);
    }

    /**
     * 计算绘制文字时的基线到中轴线的距离
     *
     * @param paint paint,需要携带字体的大小属性
     * @return 基线和centerY的距离
     */
    public int getBaseline(Paint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        int dy = (int) ((fontMetrics.bottom - fontMetrics.top)/2 - fontMetrics.bottom);
        return (getHeight() >> 1) + dy;
    }
}
