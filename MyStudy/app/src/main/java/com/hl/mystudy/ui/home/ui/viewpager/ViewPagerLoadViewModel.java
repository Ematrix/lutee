package com.hl.mystudy.ui.home.ui.viewpager;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hyhdez
 * @des
 * @date 2023/9/13 23:17
 */
public class ViewPagerLoadViewModel extends ViewModel {
    private MutableLiveData<List<String>> picUrlList = new MutableLiveData<>();

    public MutableLiveData<List<String>> getPicUrlListLive() {
        return picUrlList;
    }

    public List<String> getPicUrlList() {
        return picUrlList.getValue();
    }

    public void setData(List<String> urlList) {
        picUrlList.setValue(urlList);
    }

    public void postData(List<String> urlList) {
        picUrlList.postValue(urlList);
    }

    public List<String> getTestData() {
        List<String> dataList = new ArrayList<>(16);
        dataList.add("https://pic1.zhimg.com/v2-b9a5ba89b1e365d15838d0d5c8f36640_r.jpg");
        dataList.add("https://pic4.zhimg.com/v2-c0a8cd18ff1b1070ca14d99d7a9b179b_r.jpg");
        dataList.add("https://tse2-mm.cn.bing.net/th/id/OIP-C.OAs1l71bPomEyRJ4koUMWQHaKy?pid=ImgDet&rs=1");
        dataList.add("https://www.mlito.com/wp-content/uploads/2018/03/bf478dbc95-768x1152.jpg");
        dataList.add("https://i.hexuexiao.cn/up/84/ff/fd/7a029de32ec46d692e54c8f72bfdff84.jpg");
        dataList.add("https://www.kelagirls.com/upload/2021/07/31/1627730553058.jpg");
        dataList.add("https://i.hexuexiao.cn/up/d3/26/a8/5ae80eafb959431b801cba703ca826d3.jpg");
        dataList.add("https://www.mlito.com/wp-content/uploads/2018/03/4f1ce3fd57.jpg");
        dataList.add("https://tse2-mm.cn.bing.net/th/id/OIP-C.9GBF6JrvX2Jj8SV6EMAFlQHaLD?pid=ImgDet&rs=1");
        dataList.add("https://img.zcool.cn/community/010f4757aaf31d0000012e7ed8aff2.jpg@1280w_1l_2o_100sh.jpg");
        dataList.add("https://tse3-mm.cn.bing.net/th/id/OIP-C.7lkyLFRSBAMWof0RwSsKlQHaPO?pid=ImgDet&rs=1");
        dataList.add("https://img.zmtc.com/2019/0806/20190806063118701.jpg");
        picUrlList.setValue(dataList);
        return getPicUrlList();
    }
}
